module.exports = (app) => {
    app.get('/pagamentos', (req, res) => {
        console.log("Recebida requisicao de teste");
        res.send('OK');
    });

    app.post('/pagamentos/pagamento', (req, res) => {
        let pagamento = req.body;
        req.assert("forma_de_pagamento", "Forma de pagamento é obrigatória.").notEmpty();
        req.assert("valor", "Valor é obrigatório e deve ser um decimal.").notEmpty().isFloat();
        req.assert("moeda", "Moeda é obrigatória e deve ter 3 caracteres").notEmpty().len(3, 3);

        var errors = req.validationErrors();

        if (errors) {
            console.log("Erros de validação encontrados");
            res.status(400).send(errors);
            return;
        }
        console.log('Processando pagamentos...');
        const connection = app.config.dbConnection();
        const pagamentoDAO = new app.DAO.PagamentoDAO(connection);
        pagamento.status = 'CRIADO'
        pagamento.data = new Date;
        pagamentoDAO.salva(pagamento, (err, result) => {
            if (err) throw err;
            res.location('/pagamentos/pagamento/' + result.insertId);
            pagamento.id = result.insertId;
            let response = {
                dados_pagamento: pagamento,
                links: [
                    {
                        href: "http://localhost:5020/pagamentos/pagamento/"+ pagamento.id,
                        rel: "CONFIRMAR",
                        method:"PUT"
                    },
                    {
                        href: "http://localhost:5020/pagamentos/pagamento/"+ pagamento.id,
                        rel: "CANCELAR",
                        method:"DELETE"
                    }
                ]
            }
            res.status(201).json(response);
        })
    });

    app.put('/pagamentos/pagamento/:id', (req, res) => {
        let pagamento = {}
        let id = req.params.id;

        pagamento.id = id;
        pagamento.status = 'CONFIRMADO';

        const connection = app.config.dbConnection();
        const pagamentoDAO = new app.DAO.PagamentoDAO(connection);

        pagamentoDAO.atualizaStatus(pagamento, (err) => {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.send(pagamento);
        });
    });

    app.delete('/pagamentos/pagamento/:id', (req, res) => {
        let pagamento = {}
        let id = req.params.id;

        pagamento.id = id;
        pagamento.status = 'CANCELADO';
        
        const connection = app.config.dbConnection();
        const pagamentoDAO = new app.DAO.PagamentoDAO(connection);

        pagamentoDAO.atualizaStatus(pagamento, (err) => {
            if (err) {
                res.status(500).send(err);
                return;
            }
            res.status(204).send(pagamento);
        });
    });
}