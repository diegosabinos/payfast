const mysql = require('mysql');

function createDBConnection() {
    return mysql.createConnection({
        host: 'localhost',
        user: 'root',
        password: '3f03c0',
        database: 'payfast'
    });
}

module.exports = () => {
    return createDBConnection;
}