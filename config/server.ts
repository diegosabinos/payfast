const express = require("express");
const expressValidator = require("express-validator");
const bodyParser = require("body-parser");
const consign = require("consign");

const app = express();
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(expressValidator());
consign()
    .include('controllers')
    .then('DAO')
    .then('./config/dbConnection.ts')
    .into(app);

module.exports = app;